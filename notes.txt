Test steps

use xdt99 on pc
use nanopeb on ti with f18a

transfer by hdx over serial

run with e/a or whatever

first flight simulator:

set vdp modes

draw some tiles

flight state machine

============================================
Time 1 write to bank, 96 moves to vdp

Timer: 7576 CPU cycles - Min: 7576  Max: 7576  Average(1): 7576                                                        


2.525 ms to write a sprite pattern

Timer: 29464 CPU cycles - Min: 7576  Max: 29464  Average(2): 18520                 

7296 in vmbw per 96 bytes = 2.4 ms for data write

but this has to do 3 bit planes so time that

How many sprites update per frame max?

Assume 1/60 or 16ms: 6

How many in 100ms? 41 sprite updates
def the SAT updates completely

============================================
VDP regs

Refer to Memory Map

SITL2 EQU >0000
TILATR EQU >0300
SPRATR EQU >0700
SITL1 EQU >1000
PATTBL EQU >1800
SPTTBL EQU >3000
PATINC EQU >0800     ; pat table is 256 chars to next bit plane
SPTINC EQU >0400     ; sprite table is half size or 128 chars to next bit plane

==============================
F18A mode in Classic99

I began testing on Classic99 by using 9918 features only.

I set up the VR0-7 in a way to anticipate F18A. All good.

I added the unlock sequence, and the screen showed black.

Maybe it's the palette registers? no, tiles show up properly.
sprites use the same color, 02, green, as the tiles that show up.
make sure VR49 is >00
make sure VR51 is >20 yep that was the problem, max sprite to process was 0.

Set VR49 to 2 for  EC3. Sprites show up in awful colors, and 1x1 sz mode. Hmm.

=======================================

With F18A, what's an effective way to do scrolling in all directions on an unbounded map?

With 2x2 name table, there is a 48x64 area to draw in, so tiles scroll smoothly on and off all sides of the screen. But there is no wrapping, so when the visible area approaches the edge, you must relocate chunks of the name tables. Perhaps GPU can help there.

With 1x1 name table and scroll registers, it wraps in both directions. But there's no buffer zone to park new columns off-screen, so you need a blank column, and both sides can't have smooth entry and exit. Row-wise, can you use the 30 row mode offscreen to get some row buffers?

An optimization I see to 4x4 is to keep the written area of the name tables to 24+1 rows, so as not to chew up too much memory. Update it every row move. I end up keeping 64x48 of map chunks in RAM. (I think chunks will be 16x16.)

Another idea is to hide some rows with the BML. Since the "non scrolled" feature went away, the BML could provide a non-scrolling status panel.

With 1x2, there is a horizontal 64 columns, which would be useful if the row buffering problem were solved.

Finally, I want to do parallax scrolling, so I need TL2 and TL1. 

I planned the memory map like so, but this ain't gonna work out:


; VDP Memory Map
; 0000 | TL2 | TL2 | TL2 | TAT |
; 0400 | TL2 | TL2 | TL2 | SAT |
; 0800 | TL2 | TL2 | TL2 | ___ |
; 0C00 | TL2 | TL2 | TL2 | ___ |
;
; uhoh TL1 and TL2 have to be the same size? maybe 30 rows x 64 cols would work
;
; 1000 | TL1 | TL1 | TL1 | ___ |
; 1400 | TL1 | TL1 | TL1 | ___ |
; 1800 | PAT                   |
; 1C00 | PAT                   |
; 
; 2000 | PAT                   |
; 2400 | PAT                   |
; 2800 | PAT                   |
; 2C00 | PAT                   |
; 
; 3000 | SPT                   |
; 3400 | SPT                   |
; 3800 | SPT                   |
; 3C00 | free                  |
; 
; 4K TL2
; 6K PAT
; 3K SPT     VR29 bits set the size of SPT or PAT to 1K or 32*4*8
; 2K TL1
; 

===========================
My twisted scheduling logic
===========================

Its probably true that I work on whatever seems the most fun at the moment.

That said, here's how my projects link:

FORTI-2 is an FPGA-based music hardware for 4A that grew to emcompass SD and AMS functions.
Version 1 was so-so working, with hardware mistakes. Version 2 and 3 were about learning to use ancient PLD to reduce IO pins from 24 to 16.

512K multicart was a attempt at a conference badge you could plug in. Currently in untested PCB. PLD simulation.

Gemini was a twin-9958 peripheral, schematics only.

Udo was my high school 9995 microcomputer project. It played music. But I hadn't a clue about analog parts of 9938.

One day I was trying to understand the Geneve PAL and I looked at the schematics and a light bulb went off and I started capturing a torrent of ideas for Geneve2020. Currently in schematics, some layout, with submodules. Includes a standalone 9995 co-computer with high speed serial. 

Geneve2020 absorbs all work done on:
 FORTI-2 (FPGA, sound, SD, memory mapper), 
 512K (rewrite flash), 
 Gemini (VDP card, or more likely just use F18A mk2)
 
When I first got a Geneve, there was no software to show it off with, so I loaded GPL and Parsec. I look back on this as embarassing. Therefore it is necessary to create Parsec 2020 to show off Geneve 2020.

Parsec 2020 will be F18A required. Also 32K required. It will come in a 512k flash cart for the 4A.

========================================================
Sometimes99er font pack

https://atariage.com/forums/topic/165697-fonts/#comments

0277 0210 0144 0198


0144            1989  Commodore 64 Led Storm.
0160     is beautiful
0171     classic
0236 beautiful
0240 a Futura

0198 pretty good
0063 more legible than 0198

Going with Jupiter Ace!

========================================================

Sprite collision

update sort by y,x
any two with bounding box overlap get a more detailed coinc test
(define tighter bounding boxes for some?)
pre-generate two-sprite coinc tables?
Do it on the GPU!

==========
10/4/19
Forget BML; or maybe just actually for graphics; 
use 1x2 tile layers, leaving 2K for BML and 1K free
Let TL1 slide for now.
=============================
Scrolling 1x2 TL2
After loading the name table, set a upper and lower bound on VPO to trigger next row up or down.
If VPO>BOYU, need to fill Row 21 (wherever that is)
if VPO<BOYD
Let VPO wrap at 0? No, wrap at C0

on scroll, move both bounds by 8 pixels.

there is a global row and column into the map representing a 20x64 screen area.


Need to fill row 24 or 21


========== ships 10/4/19 ========
Parsec Mk I RENDERED
asteroid fighter game

Parsec Mk II
Viper-type 

Alien fighter
nudge to decelerate; flip when X crosses 0
feels no gravity effect
decelerates automatically for you
can only fire horizontal

Dramite lunar lander
upright
landing gear opens
X main thrust
E nose thruster?
S/D thrusters point down and 45' to side, so hold S+E to accelerate sideways.
missile: ceiling blaster (maybe homing missile?)

Gravity happens to the Dramite.

==============
round tiny velocity to zero when key is let up

