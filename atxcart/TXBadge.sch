EESchema Schematic File Version 4
LIBS:TXBadge-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5D86AFDB
P 7400 4800
F 0 "#FLG0102" H 7400 4875 50  0001 C CNN
F 1 "PWR_FLAG" H 7400 4973 50  0000 C CNN
F 2 "" H 7400 4800 50  0001 C CNN
F 3 "~" H 7400 4800 50  0001 C CNN
	1    7400 4800
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5D86B543
P 7400 4800
F 0 "#PWR0102" H 7400 4650 50  0001 C CNN
F 1 "VCC" H 7418 4973 50  0000 C CNN
F 2 "" H 7400 4800 50  0001 C CNN
F 3 "" H 7400 4800 50  0001 C CNN
	1    7400 4800
	-1   0    0    1   
$EndComp
Text Label 7250 3000 2    50   ~ 0
VDD
$Comp
L Device:C_Small C1
U 1 1 5D86C6E0
P 7250 2200
F 0 "C1" H 7342 2246 50  0000 L CNN
F 1 "18 nF" H 7342 2155 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 7250 2200 50  0001 C CNN
F 3 "~" H 7250 2200 50  0001 C CNN
	1    7250 2200
	1    0    0    -1  
$EndComp
Text Label 7250 2000 0    50   ~ 0
GND
Wire Wire Line
	7250 2000 7250 2100
$Comp
L Device:C_Small C2
U 1 1 5D86D1CC
P 4400 3700
F 0 "C2" H 4492 3746 50  0000 L CNN
F 1 "0.1 uF" H 4492 3655 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 4400 3700 50  0001 C CNN
F 3 "~" H 4400 3700 50  0001 C CNN
	1    4400 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5D86DB3C
P 4800 3700
F 0 "C3" H 4892 3746 50  0000 L CNN
F 1 "0.1 uF" H 4892 3655 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 4800 3700 50  0001 C CNN
F 3 "~" H 4800 3700 50  0001 C CNN
	1    4800 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3800 4800 3800
Wire Wire Line
	4800 3600 4400 3600
Text Label 4400 3600 2    50   ~ 0
VCC
Text Label 4400 3800 2    50   ~ 0
GND
$Comp
L power:GND #PWR0101
U 1 1 5D84D5A5
P 8750 4800
F 0 "#PWR0101" H 8750 4550 50  0001 C CNN
F 1 "GND" H 8755 4627 50  0000 C CNN
F 2 "" H 8750 4800 50  0001 C CNN
F 3 "" H 8750 4800 50  0001 C CNN
	1    8750 4800
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5D86AA20
P 8750 4800
F 0 "#FLG0101" H 8750 4875 50  0001 C CNN
F 1 "PWR_FLAG" H 8750 4973 50  0000 C CNN
F 2 "" H 8750 4800 50  0001 C CNN
F 3 "~" H 8750 4800 50  0001 C CNN
	1    8750 4800
	1    0    0    -1  
$EndComp
$Comp
L power:VSS #PWR0103
U 1 1 5D8885B6
P 7850 4800
F 0 "#PWR0103" H 7850 4650 50  0001 C CNN
F 1 "VSS" H 7868 4973 50  0000 C CNN
F 2 "" H 7850 4800 50  0001 C CNN
F 3 "" H 7850 4800 50  0001 C CNN
	1    7850 4800
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5D888A93
P 7850 4800
F 0 "#FLG0103" H 7850 4875 50  0001 C CNN
F 1 "PWR_FLAG" H 7850 4973 50  0000 C CNN
F 2 "" H 7850 4800 50  0001 C CNN
F 3 "~" H 7850 4800 50  0001 C CNN
	1    7850 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 3000 7250 2800
$Comp
L power:VDD #PWR0104
U 1 1 5D889F60
P 8300 4800
F 0 "#PWR0104" H 8300 4650 50  0001 C CNN
F 1 "VDD" H 8318 4973 50  0000 C CNN
F 2 "" H 8300 4800 50  0001 C CNN
F 3 "" H 8300 4800 50  0001 C CNN
	1    8300 4800
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 5D88B3D8
P 8300 4800
F 0 "#FLG0104" H 8300 4875 50  0001 C CNN
F 1 "PWR_FLAG" H 8300 4973 50  0000 C CNN
F 2 "" H 8300 4800 50  0001 C CNN
F 3 "~" H 8300 4800 50  0001 C CNN
	1    8300 4800
	1    0    0    -1  
$EndComp
$Comp
L Memory_Flash:SST39SF040 U2
U 1 1 5D140410
P 3500 3550
F 0 "U2" H 3500 5031 50  0000 C CNN
F 1 "AT49F040" H 3500 4940 50  0000 C CNN
F 2 "Sockets:PLCC32" H 3500 3850 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/25022B.pdf" H 3500 3850 50  0001 C CNN
	1    3500 3550
	1    0    0    -1  
$EndComp
Text Label 4100 3050 0    50   ~ 0
D0
Text Label 2900 2350 2    50   ~ 0
A15
Text Label 2900 2450 2    50   ~ 0
A14
Text Label 2900 2550 2    50   ~ 0
A13
Text Label 2900 2650 2    50   ~ 0
A12
Text Label 2900 2750 2    50   ~ 0
A11
Text Label 2900 2850 2    50   ~ 0
A10
Text Label 2900 2950 2    50   ~ 0
A9
Text Label 2900 3050 2    50   ~ 0
A8
Text Label 2900 3150 2    50   ~ 0
A7
Text Label 2900 3250 2    50   ~ 0
A6
Text Label 2900 3350 2    50   ~ 0
A5
Text Label 2900 3450 2    50   ~ 0
A4
Text Label 2900 3550 2    50   ~ 0
A3
Text Label 2900 3650 2    50   ~ 0
B7
Text Label 2900 3750 2    50   ~ 0
B6
Text Label 2900 3850 2    50   ~ 0
B5
Text Label 2900 3950 2    50   ~ 0
B4
Text Label 2900 4050 2    50   ~ 0
B3
Text Label 2900 4150 2    50   ~ 0
B2
Text Label 2900 4350 2    50   ~ 0
PGM
Text Label 2900 4550 2    50   ~ 0
ROMG*
Text Label 2900 4650 2    50   ~ 0
OE
Text Label 5500 3600 3    50   ~ 0
ROMG*
Text Label 5700 2200 1    50   ~ 0
WE*
Text Label 6500 3000 0    50   ~ 0
B7
Text Label 6500 3100 0    50   ~ 0
B6
Text Label 6500 2800 0    50   ~ 0
B5
Text Label 4100 2450 0    50   ~ 0
D6
Text Label 4100 2350 0    50   ~ 0
D7
Text Label 4100 2550 0    50   ~ 0
D5
Text Label 4100 2650 0    50   ~ 0
D4
Text Label 4100 2750 0    50   ~ 0
D3
Text Label 4100 2850 0    50   ~ 0
D2
Text Label 4100 2950 0    50   ~ 0
D1
Text Label 6500 2900 0    50   ~ 0
GND
Text Label 5600 3600 3    50   ~ 0
A14
Text Label 5600 2200 1    50   ~ 0
A13
Text Label 5500 2200 1    50   ~ 0
A12
Text Label 5100 3100 2    50   ~ 0
A11
Text Label 5100 2600 2    50   ~ 0
A10
Text Label 5800 3600 3    50   ~ 0
GND
Text Label 5700 3600 3    50   ~ 0
GND
Text Label 5100 2900 2    50   ~ 0
GND
Text Label 5900 2200 1    50   ~ 0
VCC
Text Label 5800 2200 1    50   ~ 0
VCC
Text Label 5100 2700 2    50   ~ 0
DBIN
Wire Wire Line
	7250 2300 7250 2500
Text Label 7250 2500 0    50   ~ 0
RESET
$Comp
L Device:R_US R1
U 1 1 5D86A2CF
P 7250 2650
F 0 "R1" H 7318 2696 50  0000 L CNN
F 1 "100" H 7318 2605 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7290 2640 50  0001 C CNN
F 3 "~" H 7250 2650 50  0001 C CNN
	1    7250 2650
	1    0    0    -1  
$EndComp
Text Label 5900 3600 3    50   ~ 0
A7
Text Label 6500 3200 0    50   ~ 0
OE
Text Label 6500 2700 0    50   ~ 0
B2
Text Label 6000 2200 1    50   ~ 0
B3
Text Label 6500 2600 0    50   ~ 0
B4
Text Label 5100 3000 2    50   ~ 0
A8
$Comp
L Gemini:ATF22LV10C-28J U1
U 1 1 5D142361
P 5700 3000
F 0 "U1" H 5750 4150 50  0000 L CNN
F 1 "ATF22LV10C-28J" H 5550 4050 50  0000 L CNN
F 2 "Sockets:PLCC28" H 5700 2950 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0780.pdf" H 5700 2950 50  0001 C CNN
	1    5700 3000
	1    0    0    -1  
$EndComp
Text Label 5100 2800 2    50   ~ 0
A9
Text Label 6000 3600 3    50   ~ 0
PGM
Text Label 5100 3200 2    50   ~ 0
A15
Text Label 6100 2200 1    50   ~ 0
M1
Text Label 6100 3600 3    50   ~ 0
M0
Text Notes 2300 5200 0    50   ~ 0
AT49F040 can only erase entire chip (first 16K protected)\nUse SST39SF040 for 4K eraseable sectors.\nAM29F040 can erase 64K at a time (no protected area)\n\nSoftware can read chip ID.
Text Label 5350 5800 3    50   ~ 0
B6
$Comp
L Connector:TestPoint_Alt TP5
U 1 1 5D2C95FB
P 5350 5800
F 0 "TP5" H 5300 6050 50  0001 L CNN
F 1 "B6" H 5300 6050 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Small" H 5550 5800 50  0001 C CNN
F 3 "~" H 5550 5800 50  0001 C CNN
	1    5350 5800
	1    0    0    -1  
$EndComp
Text Label 5750 5800 3    50   ~ 0
OE
$Comp
L Connector:TestPoint_Alt TP6
U 1 1 5D2C9353
P 5750 5800
F 0 "TP6" H 5700 6050 50  0001 L CNN
F 1 "OE" H 5700 6050 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Small" H 5950 5800 50  0001 C CNN
F 3 "~" H 5950 5800 50  0001 C CNN
	1    5750 5800
	1    0    0    -1  
$EndComp
Text Label 5550 5800 3    50   ~ 0
PGM
$Comp
L Connector:TestPoint_Alt TP1
U 1 1 5D2C45D8
P 5550 5800
F 0 "TP1" H 5500 6050 50  0001 L CNN
F 1 "PGM" H 5500 6050 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Small" H 5750 5800 50  0001 C CNN
F 3 "~" H 5750 5800 50  0001 C CNN
	1    5550 5800
	1    0    0    -1  
$EndComp
Text Notes 4700 5250 0    50   ~ 0
The bank register has 6 bits of address plus two mode bits. \nIt increases the address range to 2^19 = 512K.\n\nM0 M1 B2 B3 B4 B5 B6 B7\n\nM0 M1\n 0  0 Read mode\n 0  1 Command mode\n 1  0 Program even byte\n 1  1 Program odd byte\n\nCommand mode allows the 4A to write special byte sequences \nto the Flash, for erase, read chip ID, or program a byte.\nProgram modes allows 1 byte to be written to the Flash.\n\nMode bits are cleared after the expected write is complete.
Text Label 4750 5800 3    50   ~ 0
M0
$Comp
L Connector:TestPoint_Alt TP2
U 1 1 5D2CA443
P 4750 5800
F 0 "TP2" H 4700 6050 50  0001 L CNN
F 1 "M0" H 4700 6050 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Small" H 4950 5800 50  0001 C CNN
F 3 "~" H 4950 5800 50  0001 C CNN
	1    4750 5800
	1    0    0    -1  
$EndComp
Text Label 5150 5800 3    50   ~ 0
B7
$Comp
L Connector:TestPoint_Alt TP4
U 1 1 5D2CA44A
P 5150 5800
F 0 "TP4" H 5100 6050 50  0001 L CNN
F 1 "B7" H 5100 6050 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Small" H 5350 5800 50  0001 C CNN
F 3 "~" H 5350 5800 50  0001 C CNN
	1    5150 5800
	1    0    0    -1  
$EndComp
Text Label 4950 5800 3    50   ~ 0
M1
$Comp
L Connector:TestPoint_Alt TP3
U 1 1 5D2CA451
P 4950 5800
F 0 "TP3" H 4900 6050 50  0001 L CNN
F 1 "M1" H 4900 6050 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Small" H 5150 5800 50  0001 C CNN
F 3 "~" H 5150 5800 50  0001 C CNN
	1    4950 5800
	1    0    0    -1  
$EndComp
Text Notes 5000 6050 0    50   ~ 0
Test Points
$EndSCHEMATC
