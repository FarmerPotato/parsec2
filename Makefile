# Use Xdt99 tools https://endlos99.github.io/xdt99/
XDT99=/cygdrive/c/ti99/xdt99

ASM=$(XDT99)/xas99.py
DM=$(XDT99)/xdm99.py
CP=cp -p

all: obj/p2

# Output directory for HDX1 server, in V9T9 format

MOUNT=/cygdrive/c/ti99/mount

SRCS= \
    src/p2.a99 \
    src/macros.a99 \
    src/kscan.a99 \
    src/sprtest.a99 \
    src/sprites.a99 \

MAIN=src/p2.a99
LIST=obj/p2.lst
DISK=dsk/p2.dsk

# P2 will be an EA5 program file. Add it to the disk image
#	$(DM)  -a $@ -f df80 $(DISK)

obj/p2: $(SRCS) $(DISK)
	$(ASM) $(MAIN) -o $@ -i -R -L $(LIST)
	$(DM)  -a $@ -f pgm $(DISK)
	$(DM)  -a obj/p3 -f pgm $(DISK)
	$(DM)  -T $@ -f PGM -n P2 -o $(MOUNT)/P2
	$(DM)  -T obj/p3 -f PGM -n P3 -o $(MOUNT)/P3

$(DISK):
	mkdir dsk
	$(DM)  --initialize 360 $(DISK)

